
variables:
  VARS: ".env"
  PROJ_API: "$CI_API_V4_URL/projects/$CI_PROJECT_ID"

stages:
  - build
  - upload
  - release

mk-whl:
  stage: build
  before_script:
    - sed -i -r -s 's|version = "([\.0-9]+)"|version = "\1.'"$(date "+%Y.%m.%d.%H%M")"'"|' "pyproject.toml"
    - VERSION="$(grep -siPo '^\s*version\s*=\s*".+"\s*' "pyproject.toml" | tr -d '[:blank:]' | awk -F= '{print $2;}')"
    - echo "VERSION=$VERSION" > "$VARS"
    - echo "TAG=${VERSION//+/-}" >> "$VARS"
  script:
    - python3 -m build -w

    - PKG="$(find "dist" -name "*.whl" | sort | head -n 1)"
    - echo "PKG_PATH=$PKG" >> "$VARS"
    - echo "PKG_FILE=${PKG##*/}" >> "$VARS"
  artifacts:
    name: "$CI_PROJECT_NAME_$VERSION"
    paths:
      - dist/*.whl
    reports:
      dotenv: "$VARS"
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger"

upload-gitlab-whl:
  stage: upload
  needs:
    - job: mk-whl
      artifacts: true
  variables:
    TWINE_REPOSITORY_URL: "$PROJ_API/packages/pypi"
    TWINE_USERNAME: "gitlab-ci-token"
    TWINE_PASSWORD: "$CI_JOB_TOKEN"
    PKGS_URL: "$PROJ_API/packages?per_page=1&order_by=created_at&sort=desc" # page=1 is the default
  before_script:
    - test -f "$PKG_PATH"
    - if ! command -v twine;
      then pipx install twine;
      fi
  script:
    - twine upload --verbose --non-interactive "$PKG_PATH"
    - PKGS="$(curl -s --header "JOB-TOKEN:$CI_JOB_TOKEN" "$PKGS_URL")"
    - test -n "$PKGS"
    - LAST_PKG_ID="$(python3 -c "import sys, json; print(json.load(sys.stdin)[0]['id'])" <<< "$PKGS")"
    - PKG_FILES_URL="$PROJ_API/packages/$LAST_PKG_ID/package_files"
    - PKG_FILES="$(curl -s --header "JOB-TOKEN:$CI_JOB_TOKEN" "$PKG_FILES_URL")"
    - test -n "$PKG_FILES"
    - PKG_FILE_ID="$(python3 -c "import sys, json; print(json.load(sys.stdin)[0]['id'])" <<< "$PKG_FILES")"
    - echo "PKG_FILE_ID=$PKG_FILE_ID" >> "$VARS"
  artifacts:
    name: "$CI_PROJECT_NAME_$VERSION"
    reports:
      dotenv: "$VARS"
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger"

upload-testpypi-whl:
  stage: upload
  needs:
    - job: mk-whl
      artifacts: true
  before_script:
    - test -f "$PKG_PATH"
    - if ! command -v twine;
      then pipx install twine;
      fi
  script:
    - twine upload --verbose --non-interactive --config-file "$TEST_PYPI_RC"
      --repository testpypi "$PKG_PATH"
  rules:
    - if: $WITH_TEST_PYPI == "true"

upload-pypi-whl:
  stage: upload
  needs:
    - job: mk-whl
      artifacts: true
  before_script:
    - test -f "$PKG_PATH"
    - if ! command -v twine;
      then pipx install twine;
      fi
  script:
    - twine upload --verbose --non-interactive --config-file "$PYPI_RC" "$PKG_PATH"
  rules:
    - if: $WITH_PYPI == "true"

mk-release:
  stage: release
  needs:
    - job: mk-whl
      artifacts: true
    - job: upload-gitlab-whl
      artifacts: true
  variables:
    PKG_URL: "$CI_PROJECT_URL/-/package_files/$PKG_FILE_ID/download"
  script:
    - echo "$PKG_URL"
  release:
    name: "$CI_PROJECT_NAME  $VERSION"
    tag_name: "$TAG"
    tag_message: "$CI_COMMIT_TAG"
    description: "__The [package]($PKG_URL) can be installed with \\`pipx\\` or \\`pip\\`:__
      \n~~~shell\npip install $PKG_FILE\n~~~\n"
    assets:
      links:
        - name: "Download the WHL package"
          link_type: "package"
          url: "$PKG_URL"
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger"
