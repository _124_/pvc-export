### Implen spectra export(er)

__Intention:__

- extract XY (absorption spectrum) data from PVC wavescan files created by _"Implen NanoPhotometer Pearl P300"_
- export the data as a "CSV" (TAB-separated) table, original and smoothed
- find most prominent peaks in the spectrum and export them as a separate CSV table
- plot the data and save the image
- support loading of multiple PVC files, plotting them together in one figure and exporting them into a single table
- if a custom configuration file (*.yaml) provided (by the`-c`option), read it,
  merge it with the default configuration and use to style the plot(s)
- if requested (by the`--cc`option), create a custom configuration file template


__Implementation:__

- Import the default configuration from cfg.py as dict `cfg`
- Read a custom config and merge it with the default one, reconstruct missing values
- XY (absorption spectrum) array in PVC file is on the line starting with '$PVC '\
  Find and read the line, reshape the array, apply transformations specified in the config file
- Create a dict `data` holding all the read data and merged configuration
- Use `scipy.signal.savgol_filter()` to smooth the raw data
- Use`scipy.signal.find_peaks()`to find peaks in spectra
- Plot loaded data on `matplotlib.Figure()` , reserve a large part of the figure for the plot descriptions,\
  so that most of the images created with this tool have the same dimensions of the plot area.
- Create `Pillow.Image()` from the figure.
- Remove excessive white borders from the image with `Pillow.Image.crop()`
- Add borders (padding) of the size defined by config.
- Save (export) the figure image and CSV files with the loaded data


__Installation:__

- Either install the package directly from _pypi_\
  `pipx install pvc-export`,
- Or download a `.whl` file from the [latest release][] page\
  and install the package from the file\
  `pipx install <the-downloaded-file>.whl`
- In command shell, the script can be started with\
  `pvc-export` or `python3 -m pvc_export`
- In Windows, to add right click menu actions for PVC and YAML files,\
  consult the included [win-install.reg][] file.\
  Note that the suggested approach does not support combining multiple files
  into a single figure or CSV table via the right-click menu.


__Some command line examples:__

    pvc-export *.pvc
        => from all found *.pvc files produce:
        export.png       - figure
        export.csv       - plot data
        export.peaks.csv - peaks data

    pvc-export *.pvc -e <name>
        => from all found *.pvc files produce:
        <name>.png       - figure
        <name>.csv       - plot data
        <name>.peaks.csv - peaks data

    pvc-export --cc "config"
        => create "config.yaml" template,
        do not export figure or data

    pvc-export *.pvc --cc "config"
        => create "config.yaml" listing inside all found *.pvc files with the default styles assigned,
        do not export figure or data

    pvc-export -c "config.yaml"
        => use "config.yaml" to save
        figure, plot data, and peaks data with the styles and locations defined inside "config.yaml"

    pvc-export *.pvc -c "config.yaml"
        => use "config.yaml" for styles and locations to save all found .pvc files
        <name>.png       - figure,
        <name>.csv       - plot data,
        <name>.peaks.csv - peaks data,
        where <name> is set in advance in CONFIG:export:path

Variables, like `"$HOME"`, and globs, like `"*"`, are expanded in *.pvc file names, `"~"` is not.\
Globs are only expanded if a file path is not absolute.


[latest release]:
https://gitlab.com/_124_/pvc-export/-/releases/permalink/latest

[win-install.reg]:
src/pvc-export-etc/win-install.reg
